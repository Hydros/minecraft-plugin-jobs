package hydriss.minecraft.plugin.joblevels.Actions;

import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

public class Action {
    ActionType actionType;
    Block block;
    Entity entity;
    Player player;
    EntityDamageEvent damageCause;


    public Action(ActionType actionType, Block block) {
        this.actionType = actionType;
        this.block = block;
    }

    public Action(ActionType actionType, Entity entity) {
        this.actionType = actionType;
        this.entity = entity;
    }

    public Action(ActionType actionType, Entity entity, Player player) {
        this.actionType = actionType;
        this.entity = entity;
        this.player = player;
    }

    public Action(ActionType actionType, EntityDamageEvent damageCause){
        this.actionType = actionType;
        this.damageCause = damageCause;
    }

    public ActionType actionType() {
        return actionType;
    }

    public Block block() { return block; }
    public Entity entity() { return entity; }
    public Player player() { return player; }
    public EntityDamageEvent damageCause() { return damageCause; }
}
