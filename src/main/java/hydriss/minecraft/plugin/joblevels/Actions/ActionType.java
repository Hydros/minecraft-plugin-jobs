package hydriss.minecraft.plugin.joblevels.Actions;

public enum ActionType {
    BREAKING_BLOCK,
    PLACING_BLOCK,
    KILLING_ENTITY,
    TAMING_ENTITY,
    SHOOT_ENTITY,
    FEEDING_ANIMAL,
    FISHING,
    TACTICAL_FISHING,
    PLAYER_DIE,
    GIVING_ITEM
}
