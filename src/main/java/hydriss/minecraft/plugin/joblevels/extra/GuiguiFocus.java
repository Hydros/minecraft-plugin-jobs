package hydriss.minecraft.plugin.joblevels.extra;

import hydriss.minecraft.plugin.joblevels.Utils.MessageManager;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

public class GuiguiFocus {

    public  static void guiguiMeurt(PlayerDeathEvent event){
        if(event.getEntity().getName().equals("Ajirog")){
            EntityDamageEvent.DamageCause cause = event.getEntity().getLastDamageCause().getCause();
            switch (cause){
                case FIRE:
                case FIRE_TICK:
                    event.setDeathMessage(MessageManager.joueurMeurtDuFeu(event.getEntity()));
            }
        }
    }
}
