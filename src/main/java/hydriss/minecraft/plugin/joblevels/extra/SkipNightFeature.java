package hydriss.minecraft.plugin.joblevels.extra;

import hydriss.minecraft.plugin.joblevels.Utils.MessageManager;
import hydriss.minecraft.plugin.joblevels.Utils.Utils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.List;

public class SkipNightFeature implements Listener {

    double percentageThreshold;
    Plugin plugin;
    boolean skippingNight;
    List<Player> sleepingPlayers;

    public SkipNightFeature(Plugin plugin) {
        sleepingPlayers = new ArrayList<>();
        percentageThreshold = 0.5;
        skippingNight = false;
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerSleep(PlayerBedEnterEvent event) {
        if (event.getBedEnterResult() == PlayerBedEnterEvent.BedEnterResult.OK) {
            Player player = event.getPlayer();
            int nbPlayer = player.getWorld().getPlayers().size();
            sleepingPlayers.add(player);
            sleepManager(player);
            ChatColor color = Utils.playerColor(player, ChatColor.AQUA);
            player.getServer().broadcastMessage(MessageManager.playerSleep(color + player.getName(), sleepingPlayers.size(), nbPlayer));
        }
    }

    @EventHandler
    public void onPlayerLeaveBed(PlayerBedLeaveEvent event) {
        sleepingPlayers.remove(event.getPlayer());
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event){
        sleepingPlayers.remove(event.getPlayer());
    }

    @EventHandler
    public void onPlayerChangeWorld(PlayerChangedWorldEvent event){
        if(sleepingPlayers.size() > 0)
            sleepManager(sleepingPlayers.get(0));
    }

    private void sleepManager(Player player) {
        int nbPlayer = player.getWorld().getPlayers().size();
        player.getScoreboard().getTeams();
        double pct = (double) sleepingPlayers.size() / (double) nbPlayer;
        if (pct >= percentageThreshold && !skippingNight) {
            skippingNight = true;
            new BukkitRunnable() {
                @Override
                public void run() {
                    if(player.getWorld().getTime() > 12000){
                        player.getServer().broadcastMessage(MessageManager.skipNightMessage((int) (pct * 100)));
                        player.getWorld().setTime(0);
                    } else {
                        player.getServer().broadcastMessage(MessageManager.skipThunderMessage((int) (pct * 100)));
                    }
                    player.getWorld().setStorm(false);
                    skippingNight = false;
                }
            }.runTaskLater(plugin, 20);
        }
    }
}
