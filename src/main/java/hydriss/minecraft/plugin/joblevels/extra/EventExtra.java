package hydriss.minecraft.plugin.joblevels.extra;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class EventExtra implements Listener {

    @EventHandler
    public void onPlayerDie(PlayerDeathEvent event){
        GuiguiFocus.guiguiMeurt(event);
    }
}
