package hydriss.minecraft.plugin.joblevels.easyFarming;

import hydriss.minecraft.plugin.joblevels.Actions.Action;
import hydriss.minecraft.plugin.joblevels.Actions.ActionType;
import hydriss.minecraft.plugin.joblevels.Jobs.Job;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.data.Ageable;
import org.bukkit.block.data.BlockData;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

public class FarmingEvent implements Listener {

    Job[] jobsList;

    public FarmingEvent(Job[] jobsList) {
        this.jobsList = jobsList;
    }

    @EventHandler
    public void onRightClickCrop(PlayerInteractEvent event){
        if(event.getAction().equals(org.bukkit.event.block.Action.RIGHT_CLICK_BLOCK)){
            if(event.getItem() != null && isHoe(event.getItem().getType())){
                Block block = event.getClickedBlock();
                if(block != null && isCrop(block)){
                    List<ItemStack> drops = new ArrayList<>(block.getDrops(event.getPlayer().getInventory().getItemInOffHand()));
                    Player p = event.getPlayer();
                    int count = drops.get(drops.size()-1).getAmount()-1;
                    if(count > 0)
                        drops.get(drops.size() - 1).setAmount(count);
                    else
                        drops.remove(drops.size()-1);
                    for(ItemStack i : drops){
                        p.getWorld().dropItemNaturally(block.getLocation(),i);
                    }
                    Ageable ageable = ((Ageable)block.getState().getBlockData());
                    ageable.setAge(0);
                    block.setBlockData(ageable);
                    Action action = new Action(ActionType.BREAKING_BLOCK,block);
                    for(Job j : jobsList){
                        j.perform(p,action);
                    }
                    if(p.getGameMode().equals(GameMode.SURVIVAL) ||
                            p.getGameMode().equals(GameMode.ADVENTURE)){
                        ItemStack item = event.getItem();
                        double limit = 1.0/(item.getEnchantmentLevel(Enchantment.DURABILITY)+1.0);
                        double r = getRandomDouble();
                        if(r <= limit){
                            Damageable damageable = (Damageable)item.getItemMeta();
                            damageable.setDamage(damageable.getDamage()+1);
                            item.setItemMeta((ItemMeta)damageable);
                            if(((Damageable)item.getItemMeta()).getDamage() == item.getType().getMaxDurability()){
                                item.setAmount(0);
                                p.getWorld().playSound(
                                        p.getLocation(),
                                        Sound.ENTITY_ITEM_BREAK, 1F, 1F);
                            }
                        }
                    }
                }
            }
        }
    }

    private double getRandomDouble(){
        Random r = new Random();
        return r.nextDouble();
    }

    private boolean isHoe(Material mat){
        switch (mat){
            case DIAMOND_HOE:
            case GOLDEN_HOE:
            case IRON_HOE:
            case STONE_HOE:
            case WOODEN_HOE:
                return true;
        }
        return false;
    }

    private boolean isCrop(Block block){
        switch (block.getType()){
            case WHEAT:
            case CARROTS:
            case POTATOES:
            case BEETROOTS:
            case NETHER_WART:
            case COCOA:
                BlockData bdata = block.getState().getBlockData();
                if(bdata instanceof Ageable){
                    Ageable ageable = (Ageable)bdata;
                    return ageable.getAge() == ageable.getMaximumAge();
                }
        }
        return false;
    }
}
