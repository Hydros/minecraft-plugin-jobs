package hydriss.minecraft.plugin.joblevels.Jobs;

import hydriss.minecraft.plugin.joblevels.Actions.Action;
import hydriss.minecraft.plugin.joblevels.Actions.ActionType;

public class JobWoodCutter extends Job{

    private final Integer lowXp = 3;
    private final Integer mediumXp = 5;
    private final Integer highXp = 10;

    public JobWoodCutter() {
        super("Bucheron");
    }

    @Override
    protected Integer transformToXp(Action action) {
        if(action.actionType() == ActionType.BREAKING_BLOCK) {
            switch (action.block().getType()) {
                case OAK_LOG:
                case BIRCH_LOG:
                case ACACIA_LOG:
                    return highXp;
                case SPRUCE_LOG:
                    return mediumXp;
                case DARK_OAK_LOG:
                case JUNGLE_LOG:
                    return lowXp;
            }
        }
        return 0;
    }
}
