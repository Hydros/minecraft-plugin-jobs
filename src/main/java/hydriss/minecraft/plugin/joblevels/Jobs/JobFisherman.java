package hydriss.minecraft.plugin.joblevels.Jobs;

import hydriss.minecraft.plugin.joblevels.Actions.Action;
import hydriss.minecraft.plugin.joblevels.Actions.ActionType;
import org.bukkit.entity.Item;

public class JobFisherman extends Job{

    private final int baseXp = 10;
    private final int mediumXp = 30;
    private final int largeXp = 50;
    private final int extraLargeXp = 100;

    public JobFisherman() {
        super("Pecheur");
    }

    @Override
    protected Integer transformToXp(Action action) {
        if(action.actionType() == ActionType.FISHING){
            Item item = (Item)action.entity();
            switch (item.getItemStack().getType()){
                case SALMON:
                case PUFFERFISH:
                case TROPICAL_FISH:
                    return mediumXp;
                case FISHING_ROD:
                case BOW:
                case ENCHANTED_BOOK:
                case NAUTILUS_SHELL:
                case SADDLE:
                case LILY_PAD:
                    return extraLargeXp;
                default:
                    return baseXp;
            }
        }
        else if(action.actionType() == ActionType.TACTICAL_FISHING){
            switch(action.entity().getType()){
                case COD:
                case SALMON:
                case TROPICAL_FISH:
                    return largeXp;
            }
        } else if(action.actionType() == ActionType.KILLING_ENTITY){
            switch (action.entity().getType()){
                case COD:
                case SALMON:
                case TROPICAL_FISH:
                case SQUID:
                case GUARDIAN:
                    return mediumXp;
                case PUFFERFISH:
                    return largeXp;
                case ELDER_GUARDIAN:
                    return extraLargeXp;
            }
        }
        return 0;
    }
}
