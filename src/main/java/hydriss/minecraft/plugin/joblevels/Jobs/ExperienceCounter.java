package hydriss.minecraft.plugin.joblevels.Jobs;

public class ExperienceCounter {
    public static final Integer xpPerLevel = 500;
    private Integer level;
    private Integer xp;

    public ExperienceCounter() {
        this.level = 0;
        this.xp = 0;
    }

    public ExperienceCounter(Integer level, Integer xp){
        this.level = level;
        this.xp = xp;
    }

    public int addExperience(Integer xp){
        int lastLevel = level;
        this.xp += xp;
        this.level = this.xp / xpPerLevel;
        return level - lastLevel;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getXp() {
        return xp;
    }

    public void setXp(Integer xp) {
        this.xp = xp;
        this.level = this.xp / xpPerLevel;
    }
}
