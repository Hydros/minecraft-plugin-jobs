package hydriss.minecraft.plugin.joblevels.Jobs;

import com.fasterxml.jackson.databind.ObjectMapper;
import hydriss.minecraft.plugin.joblevels.Actions.Action;
import hydriss.minecraft.plugin.joblevels.Utils.MessageManager;
import hydriss.minecraft.plugin.joblevels.Utils.Utils;
import net.md_5.bungee.api.ChatMessageType;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class Job {

    protected final String dir = "./plugins/jobs/";
    protected final String extension = ".job";
    protected final Logger logger;
    protected String jobName;
    protected String xpPrecision;
    protected boolean showXpGain;
    protected Map<String, ExperienceCounter> playersExperience;

    public Job(String jobName) {
        this.jobName = jobName;
        this.logger = Logger.getLogger("logger");
        this.showXpGain = true;
        this.xpPrecision = "";
        playersExperience = new HashMap<>();
        this.load();
    }

    protected abstract Integer transformToXp(Action action);

    public ExperienceCounter getExperienceCounter(String playerName){
        return playersExperience.get(playerName);
    }

    public String getJobName(){
        return this.jobName;
    }

    public boolean isShowXpGain() {
        return showXpGain;
    }

    public void perform(Player player, Action action){
        String playerName = player.getName();
        ExperienceCounter experienceCounter;
        experienceCounter = playersExperience.get(playerName);
        if(experienceCounter == null)
            experienceCounter = new ExperienceCounter();
        Integer xpObtained = this.transformToXp(action);
        if(xpObtained != 0 && xpObtained != null){
            if(showXpGain)
                player.spigot().sendMessage(ChatMessageType.ACTION_BAR, MessageManager.xpGainedActionBar(jobName, xpObtained, xpPrecision));
            int levelGained = experienceCounter.addExperience(xpObtained);
            playersExperience.put(playerName,experienceCounter);
            this.save();
            if(levelGained != 0){
                if(experienceCounter.getLevel()%50 == 0){
                    playLvl50(player, experienceCounter.getLevel());
                } else {
                    player.getServer().broadcastMessage(MessageManager.playerLevelUp(player.getServer(), player.getDisplayName(),this.jobName, experienceCounter.getLevel()));
                }
            }
        }
    }

    public void playLvl50(Player player,int lvl){
        ChatColor color = Utils.playerColor(player,ChatColor.GOLD);
        String title = MessageManager.playerTitleLvl50(color + player.getName(),lvl,this.jobName);
        String subtitle = MessageManager.playerSubTitleLvl50(color + player.getName(),lvl,this.jobName);
        for(Player p : player.getServer().getOnlinePlayers()){
            p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_BOLT_THUNDER,0.6f,1);
            p.playSound(p.getLocation(), Sound.BLOCK_BEACON_ACTIVATE,3,1);
            p.sendTitle( title,
                    subtitle ,
                    10, 60,20);
        }
    }

    public int addXpCommand(String playerName, int xp){
        ExperienceCounter experienceCounter;
        experienceCounter = playersExperience.get(playerName);
        System.out.println(experienceCounter);
        if(experienceCounter == null){
            experienceCounter = new ExperienceCounter();
            playersExperience.put(playerName,experienceCounter);
        }
        experienceCounter.addExperience(xp);
        int newXp = experienceCounter.getXp();
        if(experienceCounter.getXp() == 0){
            playersExperience.remove(playerName);
        }
        this.save();
        return newXp;
    }

    public void setXp(String playerName, int xp){
        if(xp == 0){
            playersExperience.remove(playerName);
            return;
        }
        ExperienceCounter experienceCounter;
        experienceCounter = playersExperience.get(playerName);
        if(experienceCounter == null)
            experienceCounter = new ExperienceCounter();
        experienceCounter.setXp(xp);
    }

    public void save() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            String content = mapper.writeValueAsString(playersExperience);
            String fileName = this.jobName;
            String filePath = dir+fileName+extension;
            Path dirPath = Paths.get(dir);
            if(!Files.exists(dirPath) || !Files.isDirectory(dirPath))
                Files.createDirectory(dirPath);
            File file=new File(filePath);
            file.createNewFile(); // if file already exists will do nothing
            DataOutputStream outstream= new DataOutputStream(new FileOutputStream(file,false));
            outstream.write(content.getBytes());
            outstream.close();

        } catch (IOException e) {
            logger.log(Level.WARNING, "Faild to save a job");
            e.printStackTrace();
        }
    }

    public void load(){
        ObjectMapper mapper = new ObjectMapper();
        try {
            String fileName = this.jobName;
            String filePath = dir+fileName+extension;
            Path dirPath = Paths.get(dir);
            byte[] content = Files.readAllBytes(Paths.get(filePath));
            HashMap<String,LinkedHashMap<String,Integer>> tmp = mapper.readValue(content,HashMap.class);
            for(Map.Entry<String, LinkedHashMap<String,Integer>> e : tmp.entrySet()){
                this.playersExperience.put(e.getKey(),new ExperienceCounter(e.getValue().get("level"),e.getValue().get("xp")));
            }
        } catch (IOException e) {
            logger.log(Level.WARNING, "Faild to load a job");
        }
    }
}
