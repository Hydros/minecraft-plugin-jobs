package hydriss.minecraft.plugin.joblevels.Jobs;

import hydriss.minecraft.plugin.joblevels.Actions.Action;
import hydriss.minecraft.plugin.joblevels.Actions.ActionType;

public class JobFarmer extends Job{

    int baseXp = 3;
    int mediumXp = 5;
    int largeXp = 10;
    int extraLargeXp = 100;

    public JobFarmer() {
        super("Fermier");
    }

    @Override
    protected Integer transformToXp(Action action) {
        if(action.actionType() == ActionType.BREAKING_BLOCK){
            switch (action.block().getType()){
                case WHEAT:
                case CARROTS:
                case POTATOES:
                case BEETROOTS:
                case SUGAR_CANE:
                    return baseXp;
                case PUMPKIN:
                case MELON:
                    return mediumXp;
                case BROWN_MUSHROOM:
                case RED_MUSHROOM:
                case NETHER_WART:
                    return largeXp;
            }
        } else if(action.actionType() == ActionType.KILLING_ENTITY) {
            switch (action.entity().getType()){
                case PIG:
                case COW:
                case SHEEP:
                case RABBIT:
                case HORSE:
                case CHICKEN:
                case TURTLE:
                    return mediumXp ;
            }
        } else if (action.actionType() == ActionType.FEEDING_ANIMAL) {
            switch(action.entity().getType()){
                case PIG:
                case COW:
                case SHEEP:
                case RABBIT:
                case HORSE:
                case DONKEY:
                case CHICKEN:
                case TURTLE:
                case OCELOT:
                case WOLF:
                case CAT:
                case LLAMA:
                case PANDA:
                case BEE:
                    return baseXp;
            }

        } else if (action.actionType() == ActionType.TAMING_ENTITY) {
            switch (action.entity().getType()){
                case WOLF:
                case OCELOT:
                case CAT:
                case HORSE:
                case MULE:
                case DONKEY:
                case LLAMA:
                case PARROT:
                case FOX:
                    return extraLargeXp;
            }
        }
        return 0;
    }
}
