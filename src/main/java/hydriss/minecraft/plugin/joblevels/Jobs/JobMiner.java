package hydriss.minecraft.plugin.joblevels.Jobs;

import hydriss.minecraft.plugin.joblevels.Actions.Action;
import hydriss.minecraft.plugin.joblevels.Actions.ActionType;

public class JobMiner extends Job{

    private final int smallXp = 1;
    private final int baseXp = 3;
    private final int mediumXp = 5;
    private final int largeXp = 10;
    private final int veryLargeXp = 20;
    private final int bigXp = 50;

    public JobMiner() {
        super("Mineur");
    }

    @Override
    protected Integer transformToXp(Action action) {
        if(action.actionType() == ActionType.BREAKING_BLOCK){
            switch (action.block().getType()){
                case STONE:
                    return smallXp;
                case GRANITE:
                case DIORITE:
                case ANDESITE:
                case COAL_ORE:
                    return baseXp;
                case IRON_ORE:
                case NETHER_QUARTZ_ORE:
                    return mediumXp;
                case REDSTONE_ORE:
                case OBSIDIAN:
                    return largeXp;
                case GOLD_ORE:
                case LAPIS_ORE:
                    return veryLargeXp;
                case EMERALD_ORE:
                case DIAMOND_ORE:
                    return bigXp;
            }
        }
        return 0;
    }
}
