package hydriss.minecraft.plugin.joblevels.Jobs;

import hydriss.minecraft.plugin.joblevels.Actions.Action;
import hydriss.minecraft.plugin.joblevels.Actions.ActionType;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Tameable;

public class JobMurder extends Job{

    private final int largeXp = 200;

    public JobMurder() {
        super("Meurtrier");
    }


    @Override
    protected Integer transformToXp(Action action) {
        if(action.actionType() == ActionType.KILLING_ENTITY){
            if(action.entity().getType() == EntityType.PLAYER)
                return largeXp;
            else if(action.entity() instanceof Tameable){
                return largeXp;
            }
        }
        return 0;
    }
}
