package hydriss.minecraft.plugin.joblevels.Jobs;

import hydriss.minecraft.plugin.joblevels.Actions.Action;
import hydriss.minecraft.plugin.joblevels.Actions.ActionType;

public class JobYourDigging extends Job{

    private int smallXp = 1;
    private int baseXp = 2;
    private int mediumXp = 3;
    private int largeXp = 20;

    public JobYourDigging() {
        super("Toi tu creuse");
    }

    @Override
    protected Integer transformToXp(Action action) {
        if(action.actionType() == ActionType.BREAKING_BLOCK){
            switch (action.block().getType()){
                case DIRT:
                case GRASS_BLOCK:
                    return smallXp;
                case SAND:
                    return baseXp;
                case RED_SAND:
                    return mediumXp;
                case CLAY:
                    return largeXp;
            }
        }
        return 0;
    }
}
