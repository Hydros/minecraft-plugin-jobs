package hydriss.minecraft.plugin.joblevels.Jobs;

import hydriss.minecraft.plugin.joblevels.Actions.Action;
import hydriss.minecraft.plugin.joblevels.Actions.ActionType;

public class JobVictim extends Job{

    private int defaultXp = ExperienceCounter.xpPerLevel;

    public JobVictim() {
        super("Victime");
    }

    @Override
    protected Integer transformToXp(Action action) {
        if(action.actionType() == ActionType.PLAYER_DIE){
            switch (action.damageCause().getCause()){
                default:
                    return defaultXp;
            }
        }
        return 0;
    }
}
