package hydriss.minecraft.plugin.joblevels.Jobs;

import hydriss.minecraft.plugin.joblevels.Actions.Action;
import hydriss.minecraft.plugin.joblevels.Actions.ActionType;

public class JobSniper extends Job{

    private final double minDist = 5;

    public JobSniper() {
        super("Sniper");
    }

    @Override
    protected Integer transformToXp(Action action) {
        if(action.actionType() == ActionType.SHOOT_ENTITY){
            Double distance = action.entity().getLocation().distance(action.player().getLocation());
            this.xpPrecision = String.format(" (%.1f mètres)", distance);
            if(distance > minDist)
                return distance.intValue();
        }
        return 0;
    }
}
