package hydriss.minecraft.plugin.joblevels;

import hydriss.minecraft.plugin.joblevels.Jobs.*;
import hydriss.minecraft.plugin.joblevels.commands.JobCommand;
import hydriss.minecraft.plugin.joblevels.commands.AddOrRemoveXpCommand;
import hydriss.minecraft.plugin.joblevels.commands.SetXpCommand;
import hydriss.minecraft.plugin.joblevels.easyFarming.FarmingEvent;
import hydriss.minecraft.plugin.joblevels.extra.SkipNightFeature;
import hydriss.minecraft.plugin.joblevels.extra.EventExtra;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

public final class JobLevels extends JavaPlugin {
    Logger logger;

    @Override
    public void onEnable() {
        // Plugin startup logic
        logger = Logger.getLogger("logger");
        logger.info("Jobs Plugin just initialised");
        Job[] jobsList = {
                new JobWoodCutter(),
                new JobFarmer(),
                new JobSniper(),
                new JobMiner(),
                new JobFisherman(),
                new JobMurder(),
                new JobYourDigging(),
                new JobVictim()
        };

        getServer().getPluginManager().registerEvents(new EventManager(jobsList), this);
        getServer().getPluginManager().registerEvents(new SkipNightFeature(this), this);
        getServer().getPluginManager().registerEvents(new EventExtra(), this);
        getServer().getPluginManager().registerEvents(new FarmingEvent(jobsList), this);
        getCommand("Job").setExecutor(new JobCommand(jobsList));
        getCommand("jobsetxp").setExecutor(new SetXpCommand(jobsList));
        getCommand("jobremovexp").setExecutor(new AddOrRemoveXpCommand(jobsList));
        getCommand("jobaddxp").setExecutor(new AddOrRemoveXpCommand(jobsList));
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        logger.info("Plugin just stop");
    }
}
