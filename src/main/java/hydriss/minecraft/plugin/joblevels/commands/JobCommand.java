package hydriss.minecraft.plugin.joblevels.commands;

import hydriss.minecraft.plugin.joblevels.Jobs.ExperienceCounter;
import hydriss.minecraft.plugin.joblevels.Jobs.Job;
import hydriss.minecraft.plugin.joblevels.Utils.MessageManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

public class JobCommand implements CommandExecutor {

    Job[] jobsList;

    class JobDef {
        public String name;
        public int lvl;
        public int xp;

        public JobDef(String name, int lvl, int xp) {
            this.name = name;
            this.lvl = lvl;
            this.xp = xp;
        }

        public String toString(){
            return MessageManager.jobExperienceCommandResult(name,lvl,xp);
        }
    }

    public JobCommand(Job[] jobsList) {
        this.jobsList = jobsList;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        System.out.println(args.length);
        System.out.println(args);
        if(args.length < 1){
            List<JobDef> content = getJobsXp(sender.getName());
            if(content.size() == 0){
                sender.sendMessage(MessageManager.noJobForCurrentPlayer());
            } else {
                String msg = MessageManager.playerTitleForCommandJob(sender.getName()) + "\n";
                content.sort(JobCommand::compareOnXp);
                for(JobDef d : content) msg += d.toString() + "\n";
                sender.sendMessage(msg);
            }
        } else {
            List<JobDef> content = getJobsXp(args[0]);
            if(content.size() == 0){
                sender.sendMessage(MessageManager.badPlayerForCommandJob());
            } else {
                String msg = MessageManager.playerTitleForCommandJob(args[0]) + "\n";
                content.sort(JobCommand::compareOnXp);
                for(JobDef d : content) msg += d.toString() + "\n";
                sender.sendMessage(msg);
            }
        }
        return true;
    }

    private List<JobDef> getJobsXp(String playerName){
        List<JobDef> res = new ArrayList<>();
        for(Job j : jobsList){
            ExperienceCounter xp = j.getExperienceCounter(playerName);
            if(xp != null){
                if(j.isShowXpGain() || (!j.isShowXpGain() && xp.getLevel() > 0)){
                        res.add(new JobDef(j.getJobName(),xp.getLevel(),xp.getXp()));
                }
            }
        }
        return res;
    }

    public static int compareOnName(JobDef t1, JobDef t2){
        return t1.name.compareTo(t2.name);
    }

    public static int compareOnXp(JobDef t1, JobDef t2){
        return t2.xp-t1.xp;
    }

}
