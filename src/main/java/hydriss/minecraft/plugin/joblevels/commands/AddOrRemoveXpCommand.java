package hydriss.minecraft.plugin.joblevels.commands;

import hydriss.minecraft.plugin.joblevels.Jobs.Job;
import hydriss.minecraft.plugin.joblevels.Utils.MessageManager;
import hydriss.minecraft.plugin.joblevels.Utils.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class AddOrRemoveXpCommand implements CommandExecutor {

    Job[] jobsList;

    public AddOrRemoveXpCommand(Job[] jobsList) {
        this.jobsList = jobsList;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 3) {
            int jobNum = Utils.isJob(jobsList, args[1]);
            if (Utils.serverContainPlayer(sender.getServer(), args[0]) &&
                    jobNum != -1) {
                int xp = Integer.parseInt(args[2]);
                if(xp >= 0){
                    if(command.getName().equalsIgnoreCase("jobremovexp")){
                        xp = -xp;
                    }
                    int newXp = jobsList[jobNum].addXpCommand(args[0], xp);
                    sender.sendMessage(MessageManager.playerJobSetXp(args[0], args[1], newXp));
                    return true;
                }
            }
        }
        return false;
    }
}
