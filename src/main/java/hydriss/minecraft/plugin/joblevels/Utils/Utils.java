package hydriss.minecraft.plugin.joblevels.Utils;

import hydriss.minecraft.plugin.joblevels.Jobs.Job;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

import java.util.UUID;

public class Utils {

    public static Player getPlayerByUuid(Server server, UUID uuid) {
        for(Player p : server.getOnlinePlayers()){
            if(p.getUniqueId().equals(uuid))
                return p;
        }
        return null;
    }

    public static boolean isFish(EntityType entityType){
        switch(entityType){
            case COD:
            case PUFFERFISH:
            case SALMON:
            case TROPICAL_FISH:
                return true;
        }
        return false;
    }

    public static ChatColor playerColor(Player player, ChatColor defaultColor){
        for(Team t : player.getScoreboard().getTeams()){
            if(t.hasEntry(player.getName()))
                return t.getColor();
        }
        return defaultColor;
    }

    public static boolean isAnimalFood(Material material){
        switch (material){
            case WHEAT:
            case CARROT:
            case POTATO:
            case GOLDEN_APPLE:
            case GOLDEN_CARROT:
            case BEETROOT_SEEDS:
            case MELON_SEEDS:
            case PUMPKIN_SEEDS:
            case WHEAT_SEEDS:
            case COD:
            case SALMON:
            case TROPICAL_FISH:
            case PUFFERFISH:
            case DANDELION:
            case BEEF:
            case COOKED_BEEF:
            case CHICKEN:
            case COOKED_CHICKEN:
            case COOKED_COD:
            case MUTTON:
            case COOKED_MUTTON:
            case PORKCHOP:
            case COOKED_PORKCHOP:
            case RABBIT:
            case COOKED_RABBIT:
            case COOKED_SALMON:
            case ROTTEN_FLESH:
            case SEAGRASS:
                return true;
        }
        return false;
    }

    public static boolean serverContainPlayer(Server server, String name){
        for(Player p : server.getOnlinePlayers()){
            if(p.getName().equals(name))
                return true;
        }
        return false;
    }

    public static int isJob(Job[] jobList, String name){
        int i = 0;
        for(Job j : jobList){
            if(j.getJobName().equalsIgnoreCase(name))
                return i;
            i++;
        }
        return -1;
    }
}
