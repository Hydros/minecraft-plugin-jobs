package hydriss.minecraft.plugin.joblevels.Utils;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.entity.Player;

import java.util.Random;

public class MessageManager {

    private static String player(String name){
        return ChatColor.AQUA + name + ChatColor.WHITE;
    }

    private static String lvl(Integer name){
        return ChatColor.GOLD + name.toString() + ChatColor.WHITE;
    }

    private static String job(String name){
        return ChatColor.WHITE + name + ChatColor.WHITE;
    }

    private static String xp(Integer xp){
        return ChatColor.ITALIC + xp.toString() + ChatColor.RESET;
    }

    private static String nbPlayers(Integer nbPlayers){
        return ChatColor.RED + nbPlayers.toString() + ChatColor.RED;
    }

    public static String playerLevelUp(Server server, String player, String job, int lvl){
        if(lvl%10 == 0){
            int nbPlayers = server.getOnlinePlayers().size();
            String[] message = {
                    "Oyez oyez gentes dames et nobles damoiseaux, un heureux événement vient de se produire, " + player(player) + " vient de passer le prestigieux lvl " + lvl(lvl) + " de " + job(job) + " !",
                    "Devant près de " + nbPlayers(nbPlayers) + " joueurs, " + player(player) + " soulève son lvl " + lvl(lvl) + " de " + job(job) + " !",
                    "OULA, calme toi " + player(player) + " à vouloir passer lvl " + lvl(lvl) + " de " + job(job) + " comme ça, tu vas te blesser !",
                    "Bah alors " + player(player) + ", on essayait de passer son lvl " + lvl(lvl) + " de " + job(job) + " discrètement ?"
            };
            Random r = new Random();
            return message[r.nextInt((message.length))];
        }
        String[] message = {
                player(player) + " ce farmeur chinois vient de passer lvl " + lvl(lvl) + " en tant que " + job(job),
                "Et " + player(player) + " qui ne s'arrete plus et qui passe son lvl " + lvl(lvl) + " en tant que " + job(job) + " !",
                player(player) + " cette machine, qui obtient un lvl " + lvl(lvl) + " de " + job(job) + " bien mérité !",
                player(player) + " vient juste de passer level " + lvl(lvl) + " en " + job(job) + " !",
                "Et c'est un lvl " + lvl(lvl) + " pour " + player(player) + " en " + job(job) +" !",
                "Les qualités indéniables de " + player(player) + " lui ont permis de passer lvl " + lvl(lvl) + " dans le métier de " + job(job),
                player(player) +", ce dieu, à réussi a passer lvl " + lvl(lvl) + " en tant que " + job(job) + " !",
                player(player) + " est devenu un superbe " + job(job) + " de lvl " + lvl(lvl),
                "Aux vues des performances remarquables de " + player(player) + " en tant que " + job(job) + ", nous lui accordons un lvl " + lvl(lvl),
                "Est ce un oiseau ? Est ce un avion ? Non ! C'est " + player(player) + " qui vient de passer " + lvl(lvl) + " dans le métier de " + job(job) + " !",
                player(player) + " vient de trouver son lvl " + lvl(lvl) + " de " + job(job) + " dans un kinder surprise",
                "Mais j'ai bien l'impression que " + player(player) + " vient de passer son lvl " + lvl(lvl) + " de " + job(job) + " !",
                "Et bah alors " + player(player) + " ? On est confiné ? On passe lvl " + lvl(lvl) + " en tant que " + job(job) + " ?"
        };
        Random r = new Random();
        int min = 3;
        if(lvl > 5) {
            min = 0;
        }
        return message[r.nextInt((message.length - min)) + min];
    }

    public static String jobExperienceCommandResult(String jobName, int lvl, int xp) {
        return job(jobName) + ", lvl " + lvl(lvl) + " (" + xp + " xp)";
    }

    public static BaseComponent[] xpGainedActionBar(String jobName, Integer xpObtained, String precision){
        return TextComponent.fromLegacyText(ChatColor.GOLD + jobName + ChatColor.WHITE + " +"+xp(xpObtained)+"xp " + precision);
    }

    public static String noJobForCurrentPlayer(){
        return "Vous n'avez monté aucun job :c";
    }

    public static String badPlayerForCommandJob(){
        return "Ce joueur n'existe pas, ou est au chômage :c";
    }

    public static String playerTitleForCommandJob(String name){
        return "\nJobs de " + player(name) + " :";
    }

    public static String playerSleep(String name, int nbPlayerSleeping, int nbPlayerTotal){
        return name + ChatColor.GOLD + " dort (" + nbPlayerSleeping + "/" + nbPlayerTotal + ")";
    }

    public static String skipNightMessage(int percentage){
        return ChatColor.GREEN + Integer.toString(percentage) + "% des joueurs ont décidé de dormir. Ce serveur étant basé sur une démocratie, la nuit va être passée";
    }

    public static String skipThunderMessage(int percentage){
        return ChatColor.GREEN + Integer.toString(percentage) + "% des joueurs ont décidés de dormir. Ce server étant basé sur une démocratie, l'orage va être passé";
    }



    public static String playerJobSetXp(String jobName, String playerName, int xp){
        return "L'expérience de " + player(playerName) + " en " + job(jobName) + " à été mise à " + xp(xp);
    }

    public static String playerTitleLvl50(String player ,int lvl,String job){
        return player;
    }

    public static String playerSubTitleLvl50(String player, int lvl, String job) {
        ChatColor color = ChatColor.GRAY;
        String[] message = {
                "N'a même pas triché pour son lvl " + lvl(lvl) + color +" de " + job(job),
                "A performé pour son lvl " + lvl(lvl) + color + " de " + job(job),
                "N'a même pas peur d'être lvl " + lvl(lvl) + color + " de " + job(job),
                "Est lvl " + lvl(lvl) + color + " de " + job(job) + color + ". J'aurais jamais cru",
                "Est lvl " + lvl(lvl) + color + " de " + job(job) + color + ". On mange quoi a midi ?",
                "Déchaine mers et têmpetes pour son lvl " + lvl(lvl) + color + " de " + job(job),
                "A une détermination sans limite pour son lvl " + lvl(lvl) + color + " de " + job(job),
        };
        Random r = new Random();
        int min = 3;
        if(lvl > 5) {
            min = 0;
        }
        return color + message[r.nextInt((message.length - min)) + min] + "\n test";
    }

    public static String joueurMeurtDuFeu(Player player){
        return "Et " + Utils.playerColor(player, ChatColor.WHITE) + player.getName() + ChatColor.WHITE + " qui meurt une nouvelle fois du feu ... Il va falloir faire attention quand même ...";
    }
}
