package hydriss.minecraft.plugin.joblevels;

import hydriss.minecraft.plugin.joblevels.Actions.Action;
import hydriss.minecraft.plugin.joblevels.Actions.ActionType;
import hydriss.minecraft.plugin.joblevels.Jobs.*;
import hydriss.minecraft.plugin.joblevels.Utils.Utils;
import net.md_5.bungee.api.ChatMessageType;
import org.bukkit.Material;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import java.util.logging.Logger;

public class EventManager implements Listener {
    Job[] jobsList;
    Logger logger;

    public EventManager(Job[] jobsList) {
        this.jobsList = jobsList;
        logger = Logger.getLogger("logger");
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event){
        Player player = event.getPlayer();
        Action action = new Action(ActionType.BREAKING_BLOCK,event.getBlock());
        for (Job job: jobsList) {
            job.perform(player,action);
        }
    }
    @EventHandler
    public void onBlockPlaced(BlockPlaceEvent event){
        Player player = event.getPlayer();
        Action action = new Action(ActionType.PLACING_BLOCK,event.getBlock());
        for (Job job: jobsList) {
            job.perform(player,action);
        }
    }

    @EventHandler
    public void onEntityKilled(EntityDeathEvent event){
        LivingEntity entity = event.getEntity();
        Player player = entity.getKiller();
        if(player != null) {
            Action action = new Action(ActionType.KILLING_ENTITY,entity);
            for (Job job: jobsList) {
                job.perform(player,action);
            }
        }
    }

    @EventHandler
    public void onAnimalTamed(EntityTameEvent event){
        Player player = (Player) event.getOwner();
        Action action = new Action(ActionType.TAMING_ENTITY,event.getEntity());
        for (Job job: jobsList) {
            job.perform(player,action);
        }
    }

    @EventHandler
    public void onPlayerTouchWithArrow(EntityDamageByEntityEvent event){
        if(event.getDamager() instanceof Arrow){
            Arrow a = (Arrow) event.getDamager();
            if(a.getShooter() instanceof Player){
                Player player = (Player) a.getShooter();
                Action action = new Action(ActionType.SHOOT_ENTITY, event.getEntity(), player);
                for (Job job: jobsList) {
                    job.perform(player,action);
                }
            }
        }
    }

    @EventHandler
    public void onPlayerInteractWithEntity(PlayerInteractEntityEvent event) {
        if(event.getRightClicked() instanceof LivingEntity){
            Player player = event.getPlayer();
            LivingEntity entity = (LivingEntity)event.getRightClicked();
            Material item = player.getInventory().getItemInMainHand().getType();
            if (Utils.isAnimalFood(item)) {
                Action action = new Action(ActionType.FEEDING_ANIMAL, entity);
                for (Job job : jobsList) {
                    job.perform(player, action);
                }
            } else if (item == Material.WATER_BUCKET && !entity.hasPotionEffect(PotionEffectType.FAST_DIGGING)) {
                Action action = new Action(ActionType.TACTICAL_FISHING, entity);
                for (Job job : jobsList) {
                    job.perform(player, action);
                }
            }
        }
    }

    @EventHandler
    public void onPlayerFish(PlayerFishEvent event){
        Player player = event.getPlayer();
        if(event.getCaught() instanceof Item){
            Item item = (Item)event.getCaught();
            Action action = new Action(ActionType.FISHING, item);
            for (Job job: jobsList) {
                job.perform(player,action);
            }
        }
    }

    @EventHandler
    public void onPlayerDie(PlayerDeathEvent event){
        Player player = event.getEntity();
        Action action = new Action(ActionType.PLAYER_DIE,player.getLastDamageCause());
        for (Job job: jobsList) {
            job.perform(player,action);
        }
    }


    //-------------------------------------------------//

    @EventHandler
    public void onEntitySpawn(CreatureSpawnEvent event){
        if(Utils.isFish(event.getEntity().getType())){
            if (event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.SPAWNER_EGG) {
                event.getEntity().addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING,999999,1,true));
            }
        }
    }
}
